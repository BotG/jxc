package com.atguigu.jxc.controller;

/**
 * @Title: BotG
 * @Description:com.atguigu.jxc.controller
 * @Auther:jxc
 * @Version: 1.0
 * @create: 2023/6/4 19:12
 */

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.Map;

/**
 * @description 客户列表信息Controller
 */
@RestController
@RequestMapping("/customer")
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    @PostMapping("/list")
    public Map<String,Object> getCustomerList(Integer page, Integer rows, String  customerName){
        return customerService.getCustomerList(page,rows,customerName);
    }

    @PostMapping("/save")
    public ServiceVO saveCustomer(Customer customer){
        return customerService.saveCustomer(customer);
    }

    @PostMapping("/delete")
    public ServiceVO deleteCustomer(String  ids){
        return customerService.deleteCustomer(ids);
    }


}
