package com.atguigu.jxc.controller;

/**
 * @Title: BotG
 * @Description:com.atguigu.jxc.controller
 * @Auther:jxc
 * @Version: 1.0
 * @create: 2023/6/5 15:36
 */

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.service.DamageListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * @description 库存管理的Controller
 */
@RestController
@RequestMapping("/damageListGoods")
public class DamageListGoodsController {
    @Autowired
    private DamageListGoodsService damageListGoodsService;
    @PostMapping("/save")
    public ServiceVO saveDamageListGoods(HttpSession session, DamageList damageList, String damageListGoodsStr,String damageNumber){
        return damageListGoodsService.saveDamageListGoods(session,damageList,damageListGoodsStr,damageNumber);
    }
    @PostMapping("/list")
    public Map<String,Object> listDamageListGoods(String sTime,String eTime){
        return damageListGoodsService.listDamageListGoods(sTime,eTime);
    }
    @PostMapping("/goodsList")
    public Map<String,Object> goodsListDamageListGoods(Integer damageListId){
        return damageListGoodsService.goodsListDamageListGoods(damageListId);
    }

}
