package com.atguigu.jxc.controller;

/**
 * @Title: BotG
 * @Description:com.atguigu.jxc.controller
 * @Auther:jxc
 * @Version: 1.0
 * @create: 2023/6/5 9:21
 */

import com.atguigu.jxc.service.UnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @description 商品单位信息Controller
 */
@RestController
@RequestMapping("/unit")
public class UnitController {
    @Autowired
    private UnitService unitService;


    @PostMapping("/list")
    public Map<String,Object> getUnitList(){
        return unitService.getUnitList();
    }
}
