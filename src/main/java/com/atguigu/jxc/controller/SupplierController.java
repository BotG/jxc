package com.atguigu.jxc.controller;

/**
 * @Title: BotG
 * @Description:com.atguigu.jxc.controller
 * @Auther:jxc
 * @Version: 1.0
 * @create: 2023/6/4 16:21
 */

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @description 供应商信息Controller
 */
@RestController
@RequestMapping("/supplier")
public class SupplierController {
    @Autowired
    private SupplierService supplierService;

    //查询供应商信息
    @PostMapping("/list")
    public Map<String,Object> getSupplierList(Integer page,Integer rows,String supplierName){
        return supplierService.getSupplierList(page,rows,supplierName);
    }

    //供应商的添加或修改
    @PostMapping("/save")
    public ServiceVO saveSupplier(Supplier supplier){
        return supplierService.saveSupplier(supplier);

    }
    @PostMapping("/delete")
    public ServiceVO deleteSupplier(String  ids){
        return supplierService.deleteSupplier(ids);
    }
}
