package com.atguigu.jxc.controller;

/**
 * @Title: BotG
 * @Description:com.atguigu.jxc.controller
 * @Auther:jxc
 * @Version: 1.0
 * @create: 2023/6/5 18:54
 */

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import com.atguigu.jxc.service.OverflowListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * @description 商品报溢信息Controller
 */
@RestController
@RequestMapping("/overflowListGoods")
public class OverflowListGoodsController {

    @Autowired
    private OverflowListGoodsService overflowListGoodsService;

    @PostMapping("/save")
    public ServiceVO saveOverflowListGoods(HttpSession session,OverflowList overflowList, String overflowListGoodsStr, String overflowNumber){
        return overflowListGoodsService.saveOverflowListGoods(session,overflowList,overflowListGoodsStr,overflowNumber);
    }
    @PostMapping("/list")
    public Map<String,Object> listOverflowListGoods(String sTime,String eTime){
        return overflowListGoodsService.listOverflowListGoods(sTime,eTime);
    }
    @PostMapping("/goodsList")
    public Map<String,Object> goodsListOverflowListGoods(Integer overflowListId){
        return overflowListGoodsService.goodsListOverflowListGoods(overflowListId);
    }
}
