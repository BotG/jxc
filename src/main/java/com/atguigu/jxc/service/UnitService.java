package com.atguigu.jxc.service;

import java.util.Map;

/**
 * @Title: BotG
 * @Description:com.atguigu.jxc.service
 * @Auther:jxc
 * @Version: 1.0
 * @create: 2023/6/5 9:24
 */
public interface UnitService {
    Map<String, Object> getUnitList();
}
