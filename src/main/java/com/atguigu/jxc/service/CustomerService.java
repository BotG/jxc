package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;

import java.util.Map;

/**
 * @Title: BotG
 * @Description:com.atguigu.jxc.service
 * @Auther:jxc
 * @Version: 1.0
 * @create: 2023/6/4 19:13
 */
public interface CustomerService {
    Map<String, Object> getCustomerList(Integer page, Integer rows, String customerName);

    ServiceVO saveCustomer(Customer customer);

    ServiceVO deleteCustomer(String ids);
}
