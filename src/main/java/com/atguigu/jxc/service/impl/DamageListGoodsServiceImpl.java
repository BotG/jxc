package com.atguigu.jxc.service.impl;

import com.alibaba.fastjson.JSON;
import com.atguigu.jxc.dao.DamageListGoodsDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.DamageListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Title: BotG
 * @Description:com.atguigu.jxc.service.impl
 * @Auther:jxc
 * @Version: 1.0
 * @create: 2023/6/5 15:39
 */
@Service
public class DamageListGoodsServiceImpl implements DamageListGoodsService {
    @Autowired
    private DamageListGoodsDao damageListGoodsDao;

    //保存报损单
    @Override
    public ServiceVO saveDamageListGoods(HttpSession session, DamageList damageList, String damageListGoodsStr, String damageNumber) {
        User user =(User)session.getAttribute("currentUser");
        if (null != user){
            Integer userId = user.getUserId();
            if (null != damageList){
                damageList.setDamageNumber(damageNumber);
                damageList.setUserId(userId);
                damageListGoodsDao.saveDamageList(damageList);
                Integer damageListId = damageList.getDamageListId();
                if (damageListId != null) {
                    List<DamageListGoods> damageListGoods = JSON.parseArray(damageListGoodsStr, DamageListGoods.class);
                    if (damageListGoods != null && damageListGoods.size() > 0) {
                        for (DamageListGoods damageListGood : damageListGoods) {
                            damageListGood.setDamageListId(damageListId);
                            damageListGoodsDao.saveDamageListGoods(damageListGood);
                        }
                        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
                    }
                }
            }
        }
        return new ServiceVO(ErrorCode.PARA_TYPE_ERROR_CODE, ErrorCode.PARA_TYPE_ERROR_MESS);
    }

    @Override
    public Map<String, Object> listDamageListGoods(String sTime, String eTime) {
        List<DamageList> damageListList = damageListGoodsDao.listDamageListGoods(sTime,eTime);
        HashMap<String, Object> map = new HashMap<>();
        map.put("rows",damageListList);
        return map;
    }

    @Override
    public Map<String, Object> goodsListDamageListGoods(Integer damageListId) {
        List<DamageListGoods> damageListGoodsList = damageListGoodsDao.goodsListDamageListGoods(damageListId);
        HashMap<String, Object> map = new HashMap<>();
        map.put("rows",damageListGoodsList);
        return map;
    }


}
