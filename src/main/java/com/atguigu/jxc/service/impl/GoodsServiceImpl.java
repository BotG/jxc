package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.*;
import com.atguigu.jxc.service.CustomerReturnListGoodsService;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.SaleListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;


    //分页查询商品库存信息
    @Override
    public Map<String, Object> getListInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        Integer startNum = (page-1)*rows;
        List<Goods> goodsList = goodsDao.getListInventory(codeOrName,goodsTypeId,startNum,rows);
        if (!CollectionUtils.isEmpty(goodsList)){
            for (Goods goods : goodsList) {
                Integer saleAll = goodsDao.getSaleAll(goods.getGoodsId());
                Integer returnAll = goodsDao.getReturnAll(goods.getGoodsId());
                if (null==saleAll){
                    saleAll=0;
                }
                if (null==returnAll){
                    returnAll=0;
                }
                goods.setSaleTotal(saleAll-returnAll);
            }
        }

        Integer totalCount = goodsDao.getTotalCount(codeOrName,goodsTypeId);
        HashMap<String, Object> map = new HashMap<>();
        map.put("rows",goodsList);
        map.put("total",totalCount);
        return map;
    }

    @Override
    public Map<String, Object> getGoodsList(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {
        Integer startNum = (page-1)*rows;
        List<Goods> goodsListNew  = goodsDao.getGoodsList(startNum,rows,goodsName,goodsTypeId);
        HashMap<String, Object> map = new HashMap<>();
        map.put("total",goodsListNew.size());
        map.put("rows",goodsListNew);
        return map;
    }

    @Override
    public ServiceVO saveGoods(Goods goods) {
        goods.setInventoryQuantity(0);
        goods.setState(0);
        if(null == goods.getGoodsId()){
            Goods goods1 = goodsDao.findGoodsByName(goods.getGoodsName());
            if (goods1 != null){
                return new ServiceVO(ErrorCode.ACCOUNT_EXIST_CODE, ErrorCode.ACCOUNT_EXIST_MESS);
            }
            goodsDao.saveGoods(goods);
        } else {
            goodsDao.updateGoods(goods);
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public ServiceVO deleteGoods(Integer goodsId) {
        Goods goods = goodsDao.findGoodsById(goodsId);
        if (0 == goods.getState()){
            goodsDao.deleteGoodsById(goodsId);
        }else {
            return new ServiceVO(ErrorCode.ACCOUNT_EXIST_CODE, ErrorCode.ACCOUNT_EXIST_MESS);
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }
    //分页查询无库存商品信息
    @Override
    public Map<String, Object> getNoInventoryQuantityList(Integer page, Integer rows, String nameOrCode) {
        Integer startNum = (page-1)*rows;
        List<Goods> noInventoryQuantityList= goodsDao.getNoInventoryQuantityList(startNum,rows,nameOrCode);
        HashMap<String, Object> map = new HashMap<>();
        map.put("total",noInventoryQuantityList.size());
        map.put("rows",noInventoryQuantityList);
        return map;
    }
    //分页查询有库存商品信息
    @Override
    public Map<String, Object> getHasInventoryQuantityList(Integer page, Integer rows, String nameOrCode) {
        Integer startNum = (page-1)*rows;
        List<Goods> getHasInventoryQuantityList= goodsDao.getHasInventoryQuantityList(startNum,rows,nameOrCode);
        HashMap<String, Object> map = new HashMap<>();
        map.put("total",getHasInventoryQuantityList.size());
        map.put("rows",getHasInventoryQuantityList);
        return map;
    }
    //添加商品期初库存
    @Override
    public ServiceVO saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {
        goodsDao.saveStock(goodsId,inventoryQuantity,purchasingPrice);
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public ServiceVO deleteStock(Integer goodsId) {
        Goods goods = goodsDao.findGoodsById(goodsId);
        if (0 == goods.getState()){
            goodsDao.deleteGoodsById(goodsId);
        }else {
            return new ServiceVO(ErrorCode.ACCOUNT_EXIST_CODE, ErrorCode.ACCOUNT_EXIST_MESS);
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    //商品报警
    @Override
    public Map<String, Object> listAlarm() {
        List<Goods> goodsList = goodsDao.listAlarm();
        HashMap<String, Object> map = new HashMap<>();
        map.put("rows",goodsList);
        return map;
    }

    @Override
    public ServiceVO getCode() {
        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();
        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;
        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();
        for(int i = 4;i > intCode.toString().length();i--){
            unitCode = "0"+unitCode;
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }
}
