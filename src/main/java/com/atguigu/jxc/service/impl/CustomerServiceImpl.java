package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Title: BotG
 * @Description:com.atguigu.jxc.service.impl
 * @Auther:jxc
 * @Version: 1.0
 * @create: 2023/6/4 19:14
 */
@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    private CustomerDao customerDao;
    @Override
    public Map<String, Object> getCustomerList(Integer page, Integer rows, String customerName) {
        Integer startNum = (page-1)*rows;
        List<Supplier> supplierList = customerDao.getCustomerList(page,rows,customerName,startNum);
        HashMap<String, Object> map = new HashMap<>();
        map.put("total",supplierList.size());
        map.put("rows",supplierList);
        return map;
    }

    @Override
    public ServiceVO saveCustomer(Customer customer) {
        //判断输入的customer是否有id
        if(customer.getCustomerId() == null){
           Customer customer1 = customerDao.findCustomerByName(customer.getCustomerName());
            if (customer1 != null){
                return new ServiceVO(ErrorCode.ACCOUNT_EXIST_CODE, ErrorCode.ACCOUNT_EXIST_MESS);
            }
            customerDao.saveCustomer(customer);
        } else {
            customerDao.updateCustomer(customer);
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public ServiceVO deleteCustomer(String ids) {
        String[] splitIds = ids.split(",");
        for (int i = 0; i < splitIds.length; i++) {
            customerDao.deleteCustomer(Integer.parseInt(splitIds[i]));
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }
}
