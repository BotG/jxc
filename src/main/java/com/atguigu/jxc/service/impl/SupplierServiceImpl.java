package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Title: BotG
 * @Description:com.atguigu.jxc.service.impl
 * @Auther:jxc
 * @Version: 1.0
 * @create: 2023/6/4 16:26
 */
@Service
public class SupplierServiceImpl implements SupplierService {
    @Autowired
    private SupplierDao supplierDao;
    @Override
    public Map<String, Object> getSupplierList(Integer page, Integer rows, String supplierName) {
        Integer startNum = (page-1)*rows;
        List<Supplier> supplierList = supplierDao.getSupplierList(page,rows,supplierName,startNum);
        HashMap<String, Object> map = new HashMap<>();
        map.put("total",supplierList.size());
        map.put("rows",supplierList);
        return map;
    }

    @Override
    public ServiceVO saveSupplier(Supplier supplier) {
        // 用户ID为空时，说明是新增操作，需要先判断是否存在
        if(null == supplier.getSupplierId()){
            Supplier supplier1 = supplierDao.findSupplierByName(supplier.getSupplierName());
            if (supplier1 != null){
                return new ServiceVO(ErrorCode.ACCOUNT_EXIST_CODE, ErrorCode.ACCOUNT_EXIST_MESS);
            }
            supplierDao.saveSupplier(supplier);
        } else {
            supplierDao.updateSupplier(supplier);
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public ServiceVO deleteSupplier(String ids) {
        String[] splitIds = ids.split(",");
        for (int i = 0; i < splitIds.length; i++) {
            supplierDao.deleteSupplier(Integer.parseInt(splitIds[i]));
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }
}
