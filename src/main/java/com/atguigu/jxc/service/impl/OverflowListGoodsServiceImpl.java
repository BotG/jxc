package com.atguigu.jxc.service.impl;

import com.alibaba.fastjson.JSON;
import com.atguigu.jxc.dao.OverflowListGoodsDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.*;
import com.atguigu.jxc.service.OverflowListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Title: BotG
 * @Description:com.atguigu.jxc.service.impl
 * @Auther:jxc
 * @Version: 1.0
 * @create: 2023/6/5 19:41
 */
@Service
public class OverflowListGoodsServiceImpl implements OverflowListGoodsService {
    @Autowired
    private OverflowListGoodsDao overflowListGoodsDao;

    @Override
    public ServiceVO saveOverflowListGoods(HttpSession session, OverflowList overflowList, String overflowListGoodsStr, String overflowNumber) {
        User user =(User)session.getAttribute("currentUser");
        if (null != user) {
            Integer userId = user.getUserId();
            if (null != overflowList) {
                overflowList.setOverflowNumber(overflowNumber);
                overflowList.setUserId(userId);
                overflowListGoodsDao.saveOverflowList(overflowList);
                Integer overflowListId = overflowList.getOverflowListId();
                if (overflowListId != null) {
                    List<OverflowListGoods> overflowLists = JSON.parseArray(overflowListGoodsStr, OverflowListGoods.class);
                    if (overflowLists != null && overflowLists.size() > 0) {
                        for (OverflowListGoods overflowListGood : overflowLists) {
                            overflowListGood.setOverflowListId(overflowListId);
                            overflowListGoodsDao.saveOverflowListGoods(overflowListGood);
                        }
                        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
                    }
                }
            }
        }
        return new ServiceVO(ErrorCode.PARA_TYPE_ERROR_CODE, ErrorCode.PARA_TYPE_ERROR_MESS);    }

    @Override
    public Map<String, Object> listOverflowListGoods(String sTime, String eTime) {
        List<OverflowList>  overflowListList = overflowListGoodsDao.listOverflowListGoods(sTime,eTime);
        HashMap<String, Object> map = new HashMap<>();
        map.put("rows",overflowListList);
        return map;
    }

    @Override
    public Map<String, Object> goodsListOverflowListGoods(Integer overflowListId) {
        List<OverflowListGoods> overflowListGoodsList = overflowListGoodsDao.goodsListOverflowListGoods(overflowListId);
        HashMap<String, Object> map = new HashMap<>();
        map.put("rows",overflowListGoodsList);
        return map;
    }
}
