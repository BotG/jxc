package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.UnitDao;
import com.atguigu.jxc.entity.Unit;
import com.atguigu.jxc.service.UnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Title: BotG
 * @Description:com.atguigu.jxc.service.impl
 * @Auther:jxc
 * @Version: 1.0
 * @create: 2023/6/5 9:24
 */
@Service
public class UnitServiceImpl implements UnitService {
    @Autowired
    private UnitDao unitDao;
    @Override
    public Map<String, Object> getUnitList() {
        List<Unit> unitList = unitDao.getUnitList();
        HashMap<String, Object> map = new HashMap<>();
        map.put("rows",unitList);
        return map;
    }
}
