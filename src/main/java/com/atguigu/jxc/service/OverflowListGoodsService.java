package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.OverflowList;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * @Title: BotG
 * @Description:com.atguigu.jxc.service
 * @Auther:jxc
 * @Version: 1.0
 * @create: 2023/6/5 19:41
 */
public interface OverflowListGoodsService {
    ServiceVO saveOverflowListGoods(HttpSession session,OverflowList overflowList, String overflowListGoodsStr, String overflowNumber);

    Map<String, Object> listOverflowListGoods(String sTime, String eTime);

    Map<String, Object> goodsListOverflowListGoods(Integer overflowListId);
}
