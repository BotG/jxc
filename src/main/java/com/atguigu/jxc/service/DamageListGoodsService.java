package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * @Title: BotG
 * @Description:com.atguigu.jxc.service
 * @Auther:jxc
 * @Version: 1.0
 * @create: 2023/6/5 15:39
 */
public interface DamageListGoodsService {

    ServiceVO saveDamageListGoods(HttpSession session, DamageList damageList, String damageListGoodsStr, String damageNumber);

    Map<String, Object> listDamageListGoods(String sTime, String eTime);

    Map<String, Object> goodsListDamageListGoods(Integer damageListId);
}
