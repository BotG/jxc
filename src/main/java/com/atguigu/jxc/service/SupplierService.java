package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;

import java.util.Map;

/**
 * @Title: BotG
 * @Description:com.atguigu.jxc.service
 * @Auther:jxc
 * @Version: 1.0
 * @create: 2023/6/4 16:26
 */
public interface SupplierService {
    Map<String, Object> getSupplierList(Integer page, Integer rows, String supplierName);

    ServiceVO saveSupplier(Supplier supplier);

    ServiceVO deleteSupplier(String ids);
}
