package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Unit;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Title: BotG
 * @Description:com.atguigu.jxc.dao
 * @Auther:jxc
 * @Version: 1.0
 * @create: 2023/6/5 9:25
 */
@Mapper
public interface UnitDao {
    List<Unit> getUnitList();
}
