package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Title: BotG
 * @Description:com.atguigu.jxc.dao
 * @Auther:jxc
 * @Version: 1.0
 * @create: 2023/6/5 15:40
 */
@Mapper
public interface DamageListGoodsDao {


    void saveDamageList(DamageList damageList);

    void saveDamageListGoods(DamageListGoods damageListGood);

    List<DamageList> listDamageListGoods(String sTime, String eTime);

    List<DamageListGoods> goodsListDamageListGoods(Integer damageListId);
}
