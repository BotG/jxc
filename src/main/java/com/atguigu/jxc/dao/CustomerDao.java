package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Title: BotG
 * @Description:com.atguigu.jxc.dao
 * @Auther:jxc
 * @Version: 1.0
 * @create: 2023/6/4 19:15
 */
@Mapper
public interface CustomerDao {
    List<Supplier> getCustomerList(Integer page, Integer rows, String customerName, Integer startNum);

    Customer findCustomerByName(String customerName);

    void saveCustomer(Customer customer);

    void updateCustomer(Customer customer);

    void deleteCustomer(Integer parseInt);
}
