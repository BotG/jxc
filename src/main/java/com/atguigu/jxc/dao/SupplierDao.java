package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Title: BotG
 * @Description:com.atguigu.jxc.dao
 * @Auther:jxc
 * @Version: 1.0
 * @create: 2023/6/4 16:31
 */
@Mapper
public interface SupplierDao {

    List<Supplier> getSupplierList(Integer page, Integer rows, String supplierName, Integer startNum);

    Supplier findSupplierByName(String supplierName);

    void saveSupplier(Supplier supplier);

    void updateSupplier(Supplier supplier);


    void deleteSupplier(Integer supplierId);
}
