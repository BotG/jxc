package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @description 商品信息
 */
@Mapper
public interface GoodsDao {


    String getMaxCode();

    List<Goods> getListInventory(String codeOrName, Integer goodsTypeId, Integer startNum, Integer rows);

    Integer getSaleAll(Integer goodsId);

    Integer getReturnAll(Integer goodsId);

    Integer getTotalCount(String codeOrName, Integer goodsTypeId);

    List<Goods> getGoodsList(Integer startNum, Integer rows, String goodsName, Integer goodsTypeId);

    Goods findGoodsByName(String goodsName);

    void saveGoods(Goods goods);

    void updateGoods(Goods goods);

    Goods findGoodsById(Integer goodsId);

    void deleteGoodsById(Integer goodsId);

    List<Goods> getNoInventoryQuantityList();

    List<Goods> getNoInventoryQuantityList(Integer startNum, Integer rows, String nameOrCode);

    List<Goods> getHasInventoryQuantityList(Integer startNum, Integer rows, String nameOrCode);

    void saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice);

    List<Goods> listAlarm();
}
