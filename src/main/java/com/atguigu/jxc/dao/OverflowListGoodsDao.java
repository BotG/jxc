package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Title: BotG
 * @Description:com.atguigu.jxc.dao
 * @Auther:jxc
 * @Version: 1.0
 * @create: 2023/6/5 19:42
 */
@Mapper
public interface OverflowListGoodsDao {
    void saveOverflowList(OverflowList overflowList);

    void saveOverflowListGoods(OverflowListGoods overflowListGood);

    List<OverflowList> listOverflowListGoods(String sTime, String eTime);

    List<OverflowListGoods> goodsListOverflowListGoods(Integer overflowListId);
}
